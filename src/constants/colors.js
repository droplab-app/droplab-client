export default {
  black: '#000',
  white: '#fff',
  lightGray: '#BDD5EA',
  gray: '#c8c9c9',
  darkGray: '#333745',
  background1: '#3E7CB1',
  highlight1: '#17BEBB',
  highlight2: '#0E7C7B',
  notes: {
    0: '#e93efe',
    1: '#a63efe',
    2: '#633efe',
    3: '#3e7cfe',
    4: '#3ed2fe',
    5: '#3efef4',
    6: '#3efe76',
    7: '#bafe3e',
    8: '#f4fe3e',
    9: '#fec03e',
    10: '#fe663e',
    11: '#fe3e3e'
  }
}