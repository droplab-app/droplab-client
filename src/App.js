import React, { Component } from 'react';
import dli from 'droplab-interpreter';
import CodeMirror from 'react-codemirror';
import './App.css';
import 'codemirror/lib/codemirror.css';

import Timeline from './components/timeline/timeline';
import Editor from './components/trackEditor/editor';

const codeMirrorOptions = {
  lineNumbers: true,
};

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      dls: ''
    };

    this.updateDls = this.updateDls.bind(this);
    this.run = this.run.bind(this);
  }

  run() {
    dli.dli.interpret(this.state.dls);
  }

  updateDls(newDls) {
    this.setState({
      dls: newDls
    });
  }

  render() {
    return (
      <div className="App">
        <Timeline></Timeline>
        <Editor></Editor>
        <CodeMirror options={codeMirrorOptions} onChange={this.updateDls} value={this.state.dls}></CodeMirror>
        <button onClick={this.run}>Run</button>
      </div>
    );
  }
}

export default App;
