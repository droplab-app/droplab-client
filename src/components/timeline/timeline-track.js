import React from 'react';
import styled from 'styled-components';
import constants from 'constants/index.js';

const TrackWrapper = styled.div`
  display: flex;
  flex-grow: 1;
  flex-direction: row;
  height: 40px;
`;

const TitleWrapper = styled.div`
  display: flex;
  flex-grow: 1;
  box-sizing: border-box;
  background-color: ${props => props.selected ? constants.colors.darkGray : constants.colors.background1};
  border-bottom: 2px solid ${constants.colors.highlight1};
  align-items: center;
  padding: 0 10px;
  color: ${constants.colors.white};

  &:hover {
    color: ${constants.colors.black};
    background-color: ${constants.colors.highlight1};
    cursor: pointer;
  }
`;

const TimelineColumn = styled.div`
  display: flex;
  flex-grow: 1;
  border-bottom: 1px solid ${constants.colors.lightGray};
  box-sizing: border-box;
  background: repeating-linear-gradient(
    to right,
    ${constants.colors.white},
    ${constants.colors.white} 18px,
    ${constants.colors.lightGray} 18px,
    ${constants.colors.lightGray} 20px
  );
`;

const TitleColumn = styled.div`
  display: flex;
  width: 150px;
  border-right: 2px solid ${constants.colors.darkGray};
  box-sizing: border-box;
`;

const TimelineTrack = props => {
  return (
    <TrackWrapper>
      <TitleColumn onClick={() => props.onSelect(props.track.id)}>
        <TitleWrapper selected={props.selected}>{props.track.display}</TitleWrapper>
      </TitleColumn>
      <TimelineColumn>
      </TimelineColumn>
    </TrackWrapper>
  )
}

export default TimelineTrack;