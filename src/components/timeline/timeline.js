import React from 'react';
import styled from 'styled-components';
import { connect } from 'react-redux';
import constants from 'constants/index.js';
import TimelineTrack from './timeline-track';
import { selectTrack } from 'state/reducers/tracks/tracksActions';

const TimelineWrapper = styled.div`
  flex-grow: 1;
  background-color: ${constants.colors.white};
  border-bottom: 3px solid ${constants.colors.lightGray};
  height: 200px;
`;

const Timeline = props => {
  return (
    <TimelineWrapper>
      {props.tracks.allIds.map((id) => (
        <TimelineTrack key={id} track={props.tracks.byId[id]} onSelect={props.selectTrack} selected={props.tracks.selectedId === id}></TimelineTrack>
      ))}
    </TimelineWrapper>
  );
}

const mapStateToProps = state => ({
  tracks: state.tracks
});

const mapDispatchToProps = dispatch => ({
  selectTrack: (trackId) => dispatch(selectTrack(trackId))
});

export default connect(mapStateToProps, mapDispatchToProps)(Timeline);