import React from 'react';
import styled from 'styled-components';
import { connect } from 'react-redux';
import EditorHeader from './editor-header';
import EditorNoteTable from './editor-note-table';

const EditorWrapper = styled.div`
  display: flex;
  flex-grow: 1;
  flex-direction: column;
`;

const EditorContents = (props) => {
  return (
    <EditorWrapper>
      <EditorHeader trackTitle={props.track.display}></EditorHeader>
      <EditorNoteTable></EditorNoteTable>
    </EditorWrapper>
  );
};

const Editor = props => {

  if (props.track) {
    return EditorContents(props);
  }
  return null;
};

const mapStateToProps = state => ({
  track: state.tracks.byId[state.tracks.selectedId]
});

const mapDispatchToProps = dispatch => ({

})

export default connect(mapStateToProps, mapDispatchToProps)(Editor);