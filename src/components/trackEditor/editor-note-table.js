import React, { Component } from 'react';
import styled from 'styled-components';
import constants from 'constants/index.js';

const baseNotes = ['A', 'Bb', 'B', 'C', 'C#', 'D', 'Eb', 'E', 'F', 'F#', 'G', 'G#'];
const octaves = [6, 5, 4, 3, 2, 1];

const noteRowLeftPos = 40;

const NoteTableWrapper = styled.div`
  display: flex;
  height: 350px;
  background-color: ${constants.colors.darkGray};
  flex-direction: column;
  overflow: auto;
`;

const NoteRowWrapper = styled.div`
  display: flex;
  flex: 1 0 auto;
  flex-direction: row;
  height: 20px;
  border-bottom: 1px solid ${constants.colors.lightGray};
  color: ${constants.colors.white};
  padding: 3px 0;
`;

const NoteLeftCol = styled.div`
  flex: 0 0;
  flex-basis: ${noteRowLeftPos}px;
  border-right: 1px solid ${constants.colors.lightGray};
`;

const NoteRightCol = styled.div`
  flex: 1;
  position: relative;
  height: 100%;
`;

const NoteMarker = styled.div`
  position: absolute;
  left: ${props => `${props.pos}px`};
  background-color: ${props => constants.colors.notes[props.note]};
  width: 100px;
  height: 100%;
`

const NoteRow = ({note, hover, index, hoverNote}) => {
  return (
    <NoteRowWrapper key={index} onMouseMove={hover(index)} onMouseEnter={hover(index)}>
      <NoteLeftCol>{note}</NoteLeftCol>
      <NoteRightCol>
        {hoverNote !== null && <NoteMarker pos={hoverNote - noteRowLeftPos} note={index % baseNotes.length}></NoteMarker>}
      </NoteRightCol>
    </NoteRowWrapper>
  )
};

class EditorNoteTable extends Component {
  constructor(props) {
    super(props);
    this.state = {
      hoverPos: 0,
      hoverRow: 0
    };

    this.onRowHover = this.onRowHover.bind(this);
  }

  onRowHover(row) {
    return (e) => {
      this.setState({ 
        hoverPos: e.pageX,
        hoverRow: row
      });
    }
  }

  render() {
    return (
      <NoteTableWrapper>
        {octaves.map((octave, oi) => baseNotes.map((n, ni) => {
          let index = oi * baseNotes.length + ni;
          return NoteRow({
            note: `${n}${octave}`,
            hover: this.onRowHover,
            index,
            hoverNote: this.state.hoverRow === index ? this.state.hoverPos : null
          });
        }))}
      </NoteTableWrapper>
    );
  }
};

export default EditorNoteTable;