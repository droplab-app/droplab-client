import React from 'react';
import styled from 'styled-components';
import constants from 'constants/index.js';

const HeaderWrapper = styled.div`
  display: flex;
  flex-grow: 1;
  background-color: ${constants.colors.lightGray};
  height: 30px;
  padding: 0 10px;
  align-items: center;
  border-bottom: 2px solid ${constants.colors.darkGray};
`

const EditorHeader = props => {

  return (
    <HeaderWrapper>
      { props.trackTitle }
    </HeaderWrapper>
  );
};

export default EditorHeader;