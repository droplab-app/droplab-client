import shortid from 'shortid';
import { SELECT_TRACK } from './tracksActions';

const newTrack = (display) => ({
  display,
  id: shortid.generate()
});

const pushTrack = (state, track) => ({
  ...state,
  byId: {
    ...state.byId,
    [track.id]: track
  },
  allIds: state.allIds.concat(track.id)
});

const initialState = pushTrack({
  byId: {},
  allIds: [],
  selectedId: null
}, newTrack('default'));

const tracksReducer = (state = initialState, action) => {
  switch (action.type) {
    case SELECT_TRACK:
      let { payload: { trackId } } = action;
      return {
        ...state,
        selectedId: trackId
      };
    default: return state;
  }
}

export default tracksReducer