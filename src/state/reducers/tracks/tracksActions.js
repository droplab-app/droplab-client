export const SELECT_TRACK = 'SELECT_TRACK';
export const NEW_TRACK = 'NEW_TRACK';

export function selectTrack(trackId) {
  return {
    type: SELECT_TRACK,
    payload: {
      trackId
    }
  }
}

export function newTrack(display) {
  return {
    type: NEW_TRACK,
    payload: {
      display
    }
  }
}