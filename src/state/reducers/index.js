import { combineReducers } from 'redux';
import tracksReducer from './tracks/tracksReducer';

const rootReducer = combineReducers({
  tracks: tracksReducer
});

export default rootReducer;